# FontAwesomeHSX

This is a library for embedding [FontAwesome][fontawesome] in
[HSX][hsx]. It's currently super manually-compiled, but each icon is
an actual Haskell value, so you know at compile time if you've got a
problem.

It also handles a lot of the [FontAwesome][fontawesome] modifier
classes, at least in theory. I've only tested a few.

Anyway, I'm using it for sites, and you should too.

Also, it has sort of a translator from the [Yaml description file][ydf] from
[FontAwesome][fontawesome] to Haskell, and it's currently manual, but
probably could be done automatically. I mean it rarely changes, and it
was easy enough to just create Haskell code from it. The translator is
in the tree somewhere, too, and so when there are new releases of
[FontAwesome][fontawesome], it's pretty easy to rebuild the Haskell.

[fontawesome]: http://fontawesome.io/

[hsx]: http://hackage.haskell.org/package/hsx2hs

[ydf]: https://github.com/FortAwesome/Font-Awesome/blob/master/src/icons.yml
