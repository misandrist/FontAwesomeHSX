{-# Language TemplateHaskell #-}

module Web.FontAwesome.Types where

import Control.Lens
import Data.Text (Text, unpack, pack)
import Data.Version
import qualified Data.Set as Set
import Data.Yaml
import Data.Aeson.Types
import Numeric
import Text.ParserCombinators.ReadP
import Data.String (IsString(..))

newtype IconName = IconName {_icnName :: Text } deriving (Show, Eq, Ord)
makePrisms ''IconName

newtype IconId = IconId {_iidId :: Text } deriving (Show, Eq, Ord)
makePrisms ''IconId

newtype Unicode = Unicode {_uniChar :: Char} deriving (Show, Eq, Ord)
makePrisms ''Unicode

newtype FAVersion = FAVersion {_vVersion :: Version} deriving (Show, Eq, Ord)
makePrisms ''FAVersion

newtype Filter = Filter {_filtFilter :: Text} deriving (Show, Eq, Ord)
makePrisms ''Filter
type Filters = Set.Set Filter

newtype Category = Category {_catCategory :: Text} deriving (Show, Eq, Ord)
makePrisms ''Category
type Categories = Set.Set Category

-- | This is for the XML class attribute.
newtype FAClass = FAClass {_faClass :: Text} deriving (Show, Eq, Ord)
makePrisms ''FAClass

instance IsString FAClass where
    fromString = FAClass . pack

type FAClasses = Set.Set FAClass

data Icon = Icon {
    _icoName :: IconName,
    _icoId :: IconId,
    _icoChar :: Unicode,
    _icoVersion :: FAVersion,
    _icoFilters :: Maybe Filters,
    _icoCategories :: Categories
    } deriving (Show, Eq, Ord)
makeLenses ''Icon

data Icons = Icons {
    _icosIcons :: [Icon]
    } deriving (Show, Eq, Ord)
makeLenses ''Icons

instance FromJSON Icons where
    parseJSON (Object v) = Icons <$> v .: "icons"
    parseJSON invalid = typeMismatch "Expecting Object" invalid

instance FromJSON Icon where
    parseJSON (Object v) = Icon
        <$> v .: "name"
        <*> v .: "id"
        <*> v .: "unicode"
        <*> v .: "created"
        <*> v .:? "filter"
        <*> v .: "categories"

    parseJSON invalid = typeMismatch "Expecting Object" invalid

instance FromJSON IconName where
    parseJSON = fromJSONString IconName

instance FromJSON IconId where
    parseJSON = fromJSONString IconId

instance FromJSON Unicode where
    parseJSON = parseValue readUnicode

instance FromJSON FAVersion where
    parseJSON (Number v) =
        let
            vtxt = pack . show $ v
        in
            case readVersion vtxt of
                Left e -> fail $ unwords ["Couldn't parse version:", show v, e]
                Right vers -> return vers
    parseJSON invalid = typeMismatch "Expecting Number" invalid

instance FromJSON Filter where
    parseJSON = fromJSONString Filter

instance FromJSON Category where
    parseJSON = fromJSONString Category

fromJSONString :: forall a. (Text -> a) -> Value -> Parser a
fromJSONString p (String v) = return . p $ v
fromJSONString _ invalid = typeMismatch "Expecting String" invalid

parseValue ::
    forall a.
    (Text -> Either String a) -> Value -> Parser a
parseValue parser (String v) =
    case parser v of
        Left e -> fail e
        Right val -> return val
parseValue _ invalid =
    typeMismatch "Expecting String" invalid

readValue :: (Show v, FromJSON v) => ReadS v -> Text -> Either String v
readValue parser t =
    let
        p = filter (\(_, r) -> r == "") $ parser . unpack $ t
        ((v, x):_) = p
    in
        case p of
            [] -> Left "Parser returned no results."
            _ -> case x of
                "" -> Right v
                _ -> Left ("Parser returned extra data: " ++ x)

readUnicode :: Text -> Either String Unicode
readUnicode u =
    Unicode . toEnum <$> readValue readHex u

readVersion :: Text -> Either String FAVersion
readVersion v =
    FAVersion <$> readValue (readP_to_S parseVersion) v
