{-# Language LambdaCase, TemplateHaskell #-}

module Web.FontAwesome.Modifiers where

import Control.Lens
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Set as Set
import Web.FontAwesome.Types

class ModifierClass c where
    modClassName :: c -> FAClass

    mcClass :: Getter c FAClass
    mcClass = to (\c -> FAClass $ T.intercalate "-" ["fa", modClassName c ^. _FAClass])

data ModifierSize =
    Lg |
    X2 |
    X3 |
    X4 |
    X5
    deriving (Show, Eq, Ord)

instance ModifierClass ModifierSize where
    modClassName = \case
        Lg -> "lg"
        X2 -> "2x"
        X3 -> "3x"
        X4 -> "4x"
        X5 -> "5x"

data ModifierFixedWidth = ModifierFixedWidth deriving (Show, Eq, Ord)

instance ModifierClass ModifierFixedWidth where
    modClassName = const "fw"

data ModifierListItem =
    FALI |
    FAUL
    deriving (Show, Eq, Ord)

instance ModifierClass ModifierListItem where
    modClassName = \case
        FALI -> "li"
        FAUL -> "ul"

data ModifierSpin = ModifierSpin deriving (Show, Eq, Ord)
instance ModifierClass ModifierSpin where
    modClassName = const "spin"

data ModifierRotateFlip =
    Rotate90 |
    Rotate180 |
    Rotate270 |
    FlipHorizontal |
    FlipVertical
    deriving (Show, Eq, Ord)
instance ModifierClass ModifierRotateFlip where
    modClassName = \case
        Rotate90 -> "rotate-90"
        Rotate180 -> "rotate-180"
        Rotate270 -> "rotate-270"
        FlipHorizontal -> "flip-horizontal"
        FlipVertical -> "flip-vertical"

data Modifiers = Modifiers {
    -- | 'ModifierSize'
    _modSize :: Maybe ModifierSize,
    -- | 'ModifierFixedWidth'
    _modFixedWidth :: Maybe ModifierFixedWidth,
    -- | 'ModifierListItem'
    _modListItem :: Maybe ModifierListItem,
    -- | 'ModifierSpin'
    _modSpin :: Maybe ModifierSpin,
    -- | 'ModifierRotateFlip'
    _modRotateFlip :: Maybe ModifierRotateFlip
    } deriving (Show, Eq, Ord)
makeLenses ''Modifiers

defaultModifiers :: Modifiers
defaultModifiers = Modifiers Nothing Nothing Nothing Nothing Nothing

data ModCls = forall a. ModifierClass a => MkModCls a
pack :: ModifierClass a => a -> ModCls
pack = MkModCls

packMaybe :: ModifierClass a => Maybe a -> [ModCls]
packMaybe = maybe [] (\m -> [pack m])

modList :: Modifiers -> [ModCls]
modList mods = concat [packMaybe $ mods ^. modSize,
                       packMaybe $ mods ^. modFixedWidth,
                       packMaybe $ mods ^. modListItem,
                       packMaybe $ mods ^. modSpin,
                       packMaybe $ mods ^. modRotateFlip]

type ModifierClasses = Set.Set Text

modifiersToText :: Modifiers -> ModifierClasses
modifiersToText mods =
    let
        modl = modList mods
    in
        Set.fromList $ map (\(MkModCls m) -> m ^. mcClass ^. _FAClass) modl
