module Web.FontAwesome.IconMap (lookupIcon, iconsList, iconsToHaskell) where

import Web.FontAwesome.Types as WT
import Web.FontAwesome.IconYaml
import Data.Yaml
import qualified Data.Map as Map
import Control.Lens
import Data.Text (unpack)
import qualified Data.Text as T

iconsList :: Icons
iconsList =
    let
        (Right i) = decodeEither $ iconYaml
    in
        i

icons :: Map.Map IconId Icon
icons = Map.fromList $ map (\i -> ((i ^. icoId), i)) (iconsList ^. icosIcons)

lookupIcon :: IconId -> Maybe Icon
lookupIcon iconId = Map.lookup iconId icons

iconToHaskell :: Icon -> [String]
iconToHaskell icon =
    let
        sym =
            unpack $ T.intercalate "_" [
            "fa", (T.map (\c -> if c == '-' then '_' else c) (icon ^. icoId ^. _IconId))
            ]
    in
        [unwords [sym, "::", "Icon"],
         unwords [sym, "=", show icon],
         ""]

iconsToHaskell :: Icons -> [String]
iconsToHaskell icos =
    let
        header :: [String]
        header =
            ["{-# OPTIONS_GHC -fno-warn-warnings-deprecations #-}",
             "module Web.FontAwesome.Icons where",
             "",
             "import Web.FontAwesome.Types",
             "import Data.Set (fromList)",
             "import Data.Version",
             ""]
    in
        concat [header, concatMap iconToHaskell (icos ^. icosIcons)]
