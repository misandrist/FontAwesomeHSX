module Web.FontAwesome (
    module Web.FontAwesome.Types,
    module Web.FontAwesome.Icons,
    module Web.FontAwesome.Embed,
    module Web.FontAwesome.Modifiers
    ) where

import Web.FontAwesome.Types
import Web.FontAwesome.Icons
import Web.FontAwesome.Embed
import Web.FontAwesome.Modifiers
